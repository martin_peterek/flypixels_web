<?php

  class Database {
    
    private $host = "localhost";
    private $user = "root";
    private $password = "";
    private $database = "flypixels_web";
    public $conn;

    public function getConnection(){
      
      $this->conn = new mysqli($this->host, $this->user, $this->password, $this->database);
      $this->conn->set_charset("utf8");
      
      if(mysqli_connect_errno()){
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit();
      }

      return $this->conn;
    }

  }
  /*
  $conn = mysqli_connect("wm74.wedos.net", "a86961_fpx", "fXnrRt7f", "d86961_fpx");
  */
  ?>
<?php 
  header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");


  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Headers: X-Requested-With');
  header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PATCH');

  
  include ('../connection.php');

  $database = new Database();
  $db = $database->getConnection();
  $data = array();


  $requestMethod = $_SERVER['REQUEST_METHOD'];
  $date = date("F j, Y, g:i a");
  file_put_contents('log.txt', $date . ' --- '. $requestMethod.PHP_EOL, FILE_APPEND);

  if(isset($_GET['id'])){
    file_put_contents('log.txt', $_GET['id'].PHP_EOL, FILE_APPEND);
    
    $query = "SELECT id, nazev, poziceNaProj, datum, nahled, link, kategorie FROM portfolio WHERE id =". $_GET['id'];
    if($res = $db->prepare($query)){
      $res->execute();
      $res->bind_result($id, $name, $position, $date, $thumb, $link, $category);
      while($res->fetch()){
        $data[] = array(
          'id'=>$id,
          'nazev'=>$name,
          'poziceNaProj'=>$position,
          'datum'=>$date,
          'nahled'=>$thumb,
          'link'=>$link,
          'kategorie'=>$category
        );
      }
      $json = json_encode($data, MYSQLI_ASSOC);
      echo $json;
      $res->close();
    }
  } else {
    $query = "SELECT id, nazev, poziceNaProj, datum, nahled, link, kategorie from portfolio ORDER BY id DESC";
    if($res = $db->prepare($query)){
      $res->execute();
      $res->bind_result($id, $name, $position, $date, $thumb, $link, $category);
      while($res->fetch()){
        $data[] = array(
          'id'=>$id,
          'nazev'=>$name,
          'poziceNaProj'=>$position,
          'datum'=>$date,
          'nahled'=>$thumb,
          'link'=>$link,
          'kategorie'=>$category
        );
      }
      $json = json_encode($data, MYSQLI_ASSOC);
      echo $json;
      $res->close();
    }
  }
  
  
  if($requestMethod === 'POST' && strlen(file_get_contents('php://input')) > 0){

    $item = json_decode(file_get_contents('php://input'), true);

    file_put_contents('log.txt', file_get_contents('php://input').PHP_EOL, FILE_APPEND);

    $query = "INSERT INTO portfolio (nazev, poziceNaProj, datum, nahled, link, kategorie) VALUES(
      '" . $item['nazev'] . "',
      '" . $item['poziceNaProj'] . "',
      '" . $item['datum'] . "',
      '" . $item['nahled'] . "',
      '" . $item['link'] . "',
      '" . $item['kategorie'] . "')" ;
    if($res = mysqli_query($db, $query) === true){
    } else{
      file_put_contents('item.txt', $res.PHP_EOL, FILE_APPEND);
    }
  }

  if($requestMethod === 'PATCH' && strlen(file_get_contents('php://input')) > 0){

    $item = json_decode(file_get_contents('php://input'), true);
    
    file_put_contents('log.txt', file_get_contents('php://input').PHP_EOL, FILE_APPEND);

    $query = "UPDATE portfolio SET id = '".$item['id']."',
    nazev = '".$item['nazev']."',
    poziceNaProj = '".$item['poziceNaProj']."',
    datum = '".$item['datum']."',
    nahled = '".$item['nahled']."',
    link = '".$item['link']."',
    kategorie = '".$item['kategorie']."'
    WHERE id = " .$item['id'];
    if ($res = mysqli_query($db, $query) == true){

    }

  }

  if($requestMethod === 'DELETE'){
    $id = substr(@$_SERVER['PATH_INFO'], 1);
    file_put_contents('log.txt', 'DELETES - ' . $id.PHP_EOL, FILE_APPEND);

    
    $sql = "DELETE FROM portfolio WHERE id = ". $id ;
    
    if($res = mysqli_query($db, $sql) === true){
      file_put_contents('items.txt', $date . ' - delete - ' . $id.PHP_EOL, FILE_APPEND);
    }
  }

  $db->close();
?>
<!DOCTYPE html>
<html lang>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Zabýváme se natáčením videí různých žánrů, vytváření profesionálních leteckých záběrů a kódování webů a eshopů.">
    <meta name="keywords" content="natáčení, video, dron, letecké, letecké záběry, webdesign, web, weby">
    <meta name="author" content="Flypixels">

    <title>Flypixels - Video production, drone footage, web coding.</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Custom fonts for this theme -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
        rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,600,500,700,800,900' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="vendor/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="vendor/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="vendor/animate.css/animate.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="favicon.ico">
    <!--<link href="device-mockups/device-mockups.min.css" rel="stylesheet" type="text/css">-->

    <!-- Custom styles for this theme -->
    <!-- Uncomment the color scheme you want to use! Red is chosen by default! -->
    <link href="css/vitality-red.css" rel="stylesheet" type="text/css">

    <!-- Temporary navbar container fix -->
    <style>
        .navbar-toggler {
            z-index: 1;
        }

        @media (max-width: 576px) {
            nav>.container {
                width: 100%;
            }
        }
    </style>

    <?php 
        include ('scripts.php');
    ?>

</head>

<body id="page-top">

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-inverse navbar-expanded" id="mainNav">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">
                Flypixels
            </a>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#services">Služby</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#portfolio">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#reference">Reference</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="#contact">Kontakt</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="/admin.php">Admin</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead" style="background-image: url('img/creative/backgrounds/bg-header-2.jpg');">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 my-auto text-center text-white">
                    <img class="masthead-img img-fluid mb-3" src="img/fp/LogoFP.svg" alt="">
                    <div class="masthead-title">Flypixels</div>
                    <hr class="colored">
                    <div class="masthead-subtitle">Nafotit? Natočit? Vytvořit web? není pro nás žádný problém.</div>
                </div>
            </div>
        </div>
        <div class="scroll-down">
            <a class="btn page-scroll" href="#portfolio">
                <i class="fa fa-angle-down fa-fw"></i>
            </a>
        </div>
    </header>

    <!-- About Section -->
    <!--<section class="page-section" id="about">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-lg-6">
                    <img class="img-fluid rounded my-5" src="img/creative/about.jpg" alt="">
                </div>
                <div class="col-lg-6 text-center my-auto">
                    <h1>Kdo jsme?</h1>
                    <hr class="colored">
                    <p>I am a freelance web designer, developer, and photographer based in San Francisco, California. I have
                        a professional background in graphic design, and a degree in Computer Science from the University
                        of Central California.</p>
                    <p>Whether your next project is an online store, website for your business, or a simple brochure, I'll be
                        ready to help!</p>
                </div>
            </div>
        </div>
    </section>-->

    <!-- Services Section -->
    <section class="page-section bg-faded" id="services">
        <div class="container-fluid">
            <div class="wow fadeIn text-center">
                <h1>Co pro vás můžeme udělat?</h1>
                <!--<p class="mb-0">##</p>-->
            </div>
            <hr class="colored">
            <div class="row text-center">
                <div class="col-lg-4 col-md-6">
                    <div class="wow fadeIn px-4 pb-4 pb-lg-0 h-100" data-wow-delay=".2s">
                        <i class="fa fa-film fa-4x serviceIcon"></i>
                        <h3>Video + postprodukce</h3>
                        <p class="mb-0">Pomůžeme Vám vytvořit kvalitní video jakéhokoliv žánru.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="wow fadeIn px-4 pb-4 pb-lg-0 h-100" data-wow-delay=".4s">
                        <i class="fa fa-plane fa-4x serviceIcon"></i>
                        <h3>letecké záběry</h3>
                        <p class="mb-0">Potřebujete zachytit cokoliv, co je ze země nemožné? V tom případě jsme tady pro vás.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="wow fadeIn px-4 pb-4 pb-lg-0 h-100" data-wow-delay=".6s">
                        <i class="fa fa-code fa-4x serviceIcon"></i>
                        <h3>Webové stránky</h3>
                        <p class="mb-0">Sháníte web pro svou firmu nebo jenom chete osobní portfolio na webu? Vytvoříme co jenom si budete
                            přát.
                        </p>
                    </div>
                </div>
                <!--<div class="col-lg-3 col-md-6">
                    <div class="wow fadeIn px-4 h-100" data-wow-delay=".8s">
                        <i class="fa fa-heart fa-4x"></i>
                        <h3>Postprodukce</h3>
                        <p class="mb-0">All themes by Start Bootstrap are crafted with care. Thank you for choosing Vitality and being a customer!</p>
                    </div>
                </div>-->
            </div>
        </div>
    </section>

    <!-- Call to Action -->
    <!--<section class="call-to-action" style="background-image: url('img/creative/backgrounds/bg-quote.jpg');">
        <div class="container wow fadeIn">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <span class="quote">Good <span class="text-primary">design</span> is finding that perfect balance between the way something <span class="text-primary">looks</span> and how it <span class="text-primary">functions</span>.</span>
                    <hr class="colored">
                    <a class="btn btn-primary page-scroll" href="#services">How We Work</a>
                </div>
            </div>
        </div>
    </section>-->

    <!-- Services Section -->
    <!--<section class="page-section services" id="services">
        <div class="container">
            <div class="text-center wow fadeIn">
                <h2>My Process</h2>
                <hr class="colored">
                <p class="mb-0">Here is an overview of how I approach each new project.</p>
            </div>
            <div class="row mt-4">
                <!-- Service Item 1 
                <div class="col-lg-4 wow fadeIn" data-wow-delay=".2s">
                    <div class="media">
                        <div class="pull-left">
                            <i class="fa fa-clipboard rounded-circle"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Plan</h3>
                            <ul>
                                <li>Client interview</li>
                                <li>Gather consumer data</li>
                                <li>Create content strategy</li>
                                <li>Analyze research</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Service Item 2 
                <div class="col-lg-4 wow fadeIn" data-wow-delay=".4s">
                    <div class="media">
                        <div class="pull-left">
                            <i class="fa fa-pencil rounded-circle"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Create</h3>
                            <ul>
                                <li>Build wireframe</li>
                                <li>Gather client feedback</li>
                                <li>Code development</li>
                                <li>Marketing review</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Service Item 3 
                <div class="col-lg-4 wow fadeIn" data-wow-delay=".6s">
                    <div class="media">
                        <div class="pull-left">
                            <i class="fa fa-rocket rounded-circle"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Launch</h3>
                            <ul>
                                <li>Deploy website</li>
                                <li>Market product launch</li>
                                <li>Collect UX data</li>
                                <li>Quarterly maintenence</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->

    <!-- Portfolio Carousel Heading -->
    <!--<section class="page-section bg-faded" id="work">
        <div class="container text-center wow fadeIn">
            <h2>Work</h2>
            <hr class="colored">
            <p>Here are some examples of my work.</p>
        </div>
    </section>-->

    <!-- Portfolio Carousel -->
    <div class="portfolio-carousel wow fadeIn owl-carousel owl-theme" id="projects">

        <!-- Portfolio Carousel Item 1 -->
        <div class="item" style="background-image: url('img/creative/projects/04_velar.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 push-md-7">
                        <div class="project-details">
                            <span class="project-name">Představení Range Rover Velar</span>
                            <span class="project-description">Kamera, Fotografie</span>
                            <hr class="colored">
                            <a href="#portfolioModal1" data-toggle="modal" class="btn btn-primary">Detaily <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-7 pull-md-5 hidden-xs">
                        <div class="device-container">
                            <div class="device-mockup macbook portrait black">
                                <div class="device">
                                    <div class="screen">
                                        <iframe width="595" height="335" src="https://www.youtube.com/embed/mqSGPlv0M9w" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio Carousel Item 2 -->
        <div class="item" style="background-image: url('img/creative/projects/03_citycross.png')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 push-md-7">
                        <div class="project-details">
                            <span class="project-name">City Cross Sprint</span>
                            <span class="project-description">Kamera, Letetecké záběry, Postprodukce</span>
                            <hr class="colored">
                            <a href="#portfolioModal2" data-toggle="modal" class="btn btn-primary">Detaily<i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-7 pull-md-5 hidden-xs">
                        <div class="device-container">
                            <div class="device-mockup macbook portrait black">
                                <div class="device">
                                    <div class="screen">
                                        <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FCrossSkiingOstrava%2Fvideos%2F802943089845119%2F&show_text=0&width=595"
                                            width="595" height="335" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                                            allowTransparency="true" allowFullScreen="true"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio Carousel Item 3 -->
        <!--<div class="item" style="background-image: url('img/agency/portfolio/carousel/bg-3.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 push-md-7">
                        <div class="project-details">
                            <span class="project-name">Project Name</span>
                            <span class="project-description">Branding, Website Design</span>
                            <hr class="colored">
                            <a href="#portfolioModal3" data-toggle="modal" class="btn btn-primary">View Details <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-7 pull-md-5 hidden-xs">
                        <div class="device-container">
                            <div class="device-mockup macbook portrait black">
                                <div class="device">
                                    <div class="screen">
                                        <img class="img-fluid" src="img/agency/portfolio/carousel/screen-3a.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

        <!-- Portfolio Carousel Item 4 -->
        <div class="item" style="background-image: url('img/creative/projects/01_stezka.jpg')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 push-md-7">
                        <div class="project-details">
                            <span class="project-name">Stezka v oblacích</span>
                            <span class="project-description">Letecké záběry, Fotografie</span>
                            <hr class="colored">
                            <a href="#portfolioModal4" data-toggle="modal" class="btn btn-primary">Detaily <i class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-7 pull-md-5 hidden-xs">
                        <div class="device-container">
                            <div class="device-mockup macbook portrait black">
                                <div class="device">
                                    <div class="screen">
                                        <iframe width="595" height="335" src="https://www.youtube.com/embed/F72kIM7s9Qk" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Portfolio Grid Section -->
    <section class="page-section" id="portfolio">
        <div class="container text-center wow fadeIn">
            <h2>Portfolio</h2>
            <hr class="colored">
            <p>Projetky na kterých jsme pracovali.</p>
            <div class="controls mt-3">
                <button type="button" class="control btn btn-secondary btn-sm mx-2 mb-4" data-filter="all">Vše</button>
                <?php $cat = new Category();
                      $cat->categoryFilter(); 
                ?>
            </div>
            <div class="portfolio-grid clearfix" id="portfolioList">
                <?php $portfolio = new Portfolio();
                    $portfolio->getPortfolio();
                ?>
            </div>
        </div>
    </section>

    <!-- Testomonials Section -->
    <section class="page-section testimonials bg-inverse text-white" id="reference">
        <div class="container text-center wow fadeIn">
            <h1>reference</h1>
            <hr class="colored">
            <div class="row">
                <div class="col-lg-12 owl-reference">
                    <div class="item"><img class="corporationImage" src="img/creative/reference/06_citycross.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/03_bobf.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/01_tarosnova.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/04_garage.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/05_cvg.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/08_presentigo.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/02_v12.png" /></div>
                    <div class="item"><img class="corporationImage" src="img/creative/reference/07_mixiew.png" /></div>
                </div>
            </div>
        </div>
    </section>

    <!-- Newsletter Signup Call to Action -->
    <!--<section class="page-section signup-form bg-inverse text-white">
        <div class="container text-center">
            <h3 class="m0">Subscribe to our newsletter!</h3>
            <hr class="colored">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <!-- MailChimp Signup Form 
                    <div id="mc_embed_signup">
                        <!-- Replace the form action in the line below with your MailChimp embed action! Visit the documentation for additional instructions! 
                        <form role="form" action="//startbootstrap.us3.list-manage.com/subscribe/post?u=531af730d8629808bd96cf489&amp;id=afb284632f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                            <div class="input-group input-group-lg">
                                <input type="email" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Email address...">
                                <span class="input-group-btn">
                                    <button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary">Subscribe!</button>
                                </span>
                            </div>
                            <div id="mce-responses">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>
                        </form>
                    </div>
                    <!-- End MailChimp Signup Form
                </div>
            </div>
        </div>
    </section>-->

    <!-- Contact Section -->
    <section class="page-section" id="contact">
        <div class="container wow fadeIn">
            <div class="text-center">
                <h2>Napište nám</h2>
                <hr class="colored">
                <p>Pomůžeme Vám s Vaším projektem.</p>
            </div>
            <div class="row mt-4">
                <div class="col-lg-8 offset-lg-2">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row control-group">
                            <div class="form-group col-12 floating-label-form-group controls">
                                <label>Jméno</label>
                                <input type="text" class="form-control" placeholder="Jméno" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-12 floating-label-form-group controls">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-12 floating-label-form-group controls">
                                <label>Telefonní číslo</label>
                                <input type="tel" class="form-control" placeholder="Telefonní číslo" id="phone">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-12 floating-label-form-group controls">
                                <label>Zpráva</label>
                                <textarea rows="5" class="form-control" placeholder="Zpráva" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-12">
                                <button type="submit" class="btn btn-secondary">Odeslat</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Block Button Call to Action -->
    <!--<a class="btn btn-block btn-full-width" href="https://wrapbootstrap.com/theme/vitality-multipurpose-one-page-theme-WB02K3KK3">Buy Vitality Now!</a>-->

    <!-- Footer -->
    <footer class="footer" style="background-image: url('img/creative/backgrounds/bg-footer.jpg')">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-4 footer-contact-details">
                    <h4><i class="fa fa-phone"></i>Telefon</h4>
                    <p>Dron: <a href="tel:608411580">+420 608 411 580</a></p>
                    <p>Kamera: <a href="tel:733101525">+420 733 101 525</a></p>
                </div>
                <div class="col-md-4 footer-contact-details socialMedia">
                    <!--<h4> Sociální media</h4>
                    <p><a href=""><i class="fa fa-facebook-square"></i></a></p>
                    <p><a href=""><i class="fa fa-instagram"></i></a></p>-->
                </div>
                <div class="col-md-4 footer-contact-details">
                    <h4><i class="fa fa-envelope"></i> Email</h4>
                    <p><a href="mailto:info@flypixels.cz">info@flypixels.cz</a></p>
                    <p>Dron: <a href="mailto:pagac@flypixels.cz">pagac@flypixels.cz</a></p>
                    <p>Kamera: <a href="mailto:peterek@flypixels.cz">peterek@flypixels.cz</a></p>
                </div>
            </div>
            <div class="row footer-social">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/flypixelscz/"><i class="fa fa-facebook fa-fw fa-2x"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/flypixelscz/"><i class="fa fa-instagram fa-fw fa-2x"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <p class="copyright">&copy; Flypixels</p>
        </div>
    </footer>

    <!-- Example Modal 2: Corresponds with Portfolio Carousel Item 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/creative/projects/04_velar.jpg')">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2">
                                <h2>Představení Range Rover Velar</h2>
                                <hr class="colored">
                                <p></p>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <div class="device-mockup macbook portrait black">
                                    <div class="device">
                                        <div class="screen">

                                            <!-- Modal Mockup Option 1: Single Image (Uncomment Below to Use) -->
                                            <!-- <img src="img/agency/portfolio/carousel/screen-2a.jpg" class="img-fluid" alt=""> -->

                                            <!-- Modal Mockup Option 2: Carousel (Example In Use Below) -->
                                            <div class="mockup-carousel">
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/01_velar.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/02_velar.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/03_velar.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/04_velar.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <ul class="list-inline item-details">
                                    <li>Klient: <strong>Best of British Cars Ostrava</strong>
                                    </li>
                                    <li>Spolupráce s: <strong><a class="colab" href="https://www.facebook.com/SALAMONMEDIA/">Salamon production</a></strong>
                                    </li>
                                    <li>Datum: <strong>Červen 2017</strong>
                                    </li>
                                    <li>Služby: <strong>Kamera + Fotografie</strong>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Example Modal 3: Corresponds with Portfolio Carousel Item 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/creative/projects/03_citycross.png')">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2">
                                <h2>City Cross Sprint</h2>
                                <hr class="colored">
                                <p></p>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <div class="device-mockup macbook portrait black">
                                    <div class="device">
                                        <div class="screen">

                                            <!-- Modal Mockup Option 1: Single Image (Uncomment Below to Use) -->
                                            <!-- <img src="img/agency/portfolio/carousel/screen-2a.jpg" class="img-fluid" alt=""> -->

                                            <!-- Modal Mockup Option 2: Carousel (Example In Use Below) -->
                                            <div class="mockup-carousel">
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/01_citycross.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/02_citycross.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/03_citycross.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/04_citycross.jpg" class="img-fluid" alt="">
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <ul class="list-inline item-details">
                                    <li>Klient: <strong>Seven Days Agency</strong>
                                    </li>
                                    <li>Datum: <strong>Leden 2017</strong>
                                    </li>
                                    <li>Služby: <strong>Kamera, Letecké záběry, Postprodukce</strong>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Example Modal 4: Corresponds with Portfolio Carousel Item 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/agency/portfolio/carousel/bg-4.jpg')">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2">
                                <h2>Project Name</h2>
                                <hr class="colored">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos modi in tenetur vero voluptatum
                                    sapiente dolores eligendi nemo iste ea. Omnis, odio enim sint quam dolorum dolorem. Nostrum,
                                    minus, ad.</p>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <div class="device-mockup macbook portrait black">
                                    <div class="device">
                                        <div class="screen">

                                            <!-- Modal Mockup Option 1: Single Image (Example In Use Below) -->
                                            <img src="img/agency/portfolio/carousel/screen-4.jpg" class="img-fluid" alt="">

                                            <!-- Modal Mockup Option 2: Carousel (Uncomment Below to Use) -->
                                            <!-- <div class="mockup-carousel">
                                            <div class="item">
                                                <img src="img/agency/portfolio/carousel/screen-1a.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item">
                                                <img src="img/agency/portfolio/carousel/screen-2a.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="item">
                                                <img src="img/agency/portfolio/carousel/screen-3a.jpg" class="img-fluid" alt="">
                                            </div>
                                        </div> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <ul class="list-inline item-details">
                                    <li>Client: <strong>Start Bootstrap</strong>
                                    </li>
                                    <li>Date: <strong>April 2015</strong>
                                    </li>
                                    <li>Service: <strong>Web Development</strong>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modals -->
    <!-- Example Modal 1: Corresponds with Portfolio Carousel Item 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true" style="background-image: url('img/agency/portfolio/carousel/bg-1.jpg')">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2">
                                <h2>Stezka v oblacích</h2>
                                <hr class="colored">
                                <p>Nachází se poblíž horské chaty Slaměnka, nedaleko horní stanice lanovky Sněžník, v nadmořské
                                    výšce 1116 m. n. m. Nahoru se dostanete lanovkou, pěšky nebo na kole. Kočárky a kola
                                    přepravujeme na lanovce zdarma, s kočárkem se pohodlně dostanete až na vrchol stezky
                                    po dřevěné lávce. Samotná stezka je vysoká 55 metrů.</p>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <div class="device-mockup macbook portrait black">
                                    <div class="device">
                                        <div class="screen">

                                            <!-- Modal Mockup Option 1: Single Image (Uncomment Below to Use) -->
                                            <!-- <img src="img/agency/portfolio/carousel/screen-1a.jpg" class="img-fluid" alt=""> -->

                                            <!-- Modal Mockup Option 2: Carousel (Example In Use Below) -->
                                            <div class="mockup-carousel">
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/01_stezka.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/02_stezka.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/03_stezka.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="item">
                                                    <img src="img/creative/projects-modal/04_stezka.jpg" class="img-fluid" alt="">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 offset-lg-2">
                                <ul class="list-inline item-details">
                                    <li>Client: <strong>Taros Nova s.r.o</strong>
                                    </li>
                                    <li>Date: <strong>Říjen 2015 - Prosinec 2015</strong>
                                    </li>
                                    <li>Service: <strong>Letecké záběry stavby + Fotografie</strong>
                                    </li>
                                </ul>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/tether/tether.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="vendor/owl-carousel/owl.carousel.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="vendor/vide/jquery.vide.min.js"></script>
    <script src="vendor/mixitup/mixitup.min.js"></script>
    <script src="vendor/wowjs/wow.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/contact_me.js"></script>
    <script src="js/jqBootstrapValidation.js"></script>

    <!-- Custom scripts for this theme -->
    <script src="js/vitality.js"></script>
    <script src="js/vitality-mixitup.js"></script>

</body>

</html>
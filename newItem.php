<div class="l-inputs">
  <form method="post">
    <div class="l-input">
      <label class="label" for="nazev">Název</label>
      <input class="input input-name" name="nazev" id="nazev" type="text">
    </div>
    <div class="l-input">
      <label class="label" for="pozice">Pozice</label>
      <input class="input input-position" name="pozice" id="pozice" type="text">
    </div>
    <div class="l-input">
      <label class="label" for="datum">Datum</label>
      <input class="input input-position" name="datum" id="datum" type="text">
    </div>
    <div class="l-input textarea">
      <label class="label" for="link">Link</label>
      <textarea class="input input-textarea" name="link" id="link" rows="4"></textarea>
    </div>
    <div class="l-input">
      <label class="label" for="nahled">Náhled</label>
      <input class="input" name="nahled" type="text" id="nahled">
    </div>
    <div class="l-input">
      <label class="label" for="kategorie">Kategorie</label>
      <select class="input" name="kategorie" type="text" id="kategorie">
        <?php categoryList('select') ?>
      </select>
    </div>
    <button class="btn_save" name="saveItem" type="submit">Uložit</button>
  </form>
</div>
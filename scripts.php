<?php
  include_once ('connection.php');

  class Category {

    private $database;
    protected $db;

    public function __construct(){
      $this->database = new Database();
      $this->db = $this->database->getConnection();
    }

    public function insertInCategory($klic, $nazev){
      mysqli_set_charset($this->db, "utf8");
      $sql = "INSERT INTO ckategorie (klic, nazev) VALUES(
        '" . $klic . "',
        '" . $nazev . "')";
      if($res = mysqli_query($this->Gdb, $sql)){
      }
    }

    public function categoryList($place) {
      $query = "SELECT * FROM ckategorie";

      if($res = mysqli_query($this->Gdb, $query)){
        while($row = mysqli_fetch_row($res)){
          if($place === 'select'){
            echo "<option value=". $row['1'] .">". $row['2'] ."</option>";
            } else {
              echo '
              <div class="categoryList_line">
                <div class="categoryList_cell categoryList_cell-key">'. $row['1'] .'</div>
                <div class="categoryList_cell categoryList_cell-name">'. $row['2'].'</div>
                <div class="categoryList_cell categoryList_cell-delete">
                  <a href="admin.php?category='. $row['0'] .'" type="submit"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </div>
              </div>';
            } 
        }
        mysqli_free_result($res);
      }
    }

    public function categoryFilter(){
      $query = "SELECT * FROM ckategorie";
      if($res = mysqli_query($this->db, $query)){
          while($row = mysqli_fetch_row($res)){
            echo "<button type=\"button\" class=\"control btn btn-secondary btn-sm mx-2 mb-4\" data-filter=\".". $row['1'] ."\">". $row['2'] ."</button>";
        }
      }
    }
  }


  class Portfolio{

    private $database;
    protected $db;

    public function __construct(){
      $this->database = new Database();
      $this->db = $this->database->getConnection();
    }

    public function getPortfolio(){
      $query = "SELECT * FROM portfolio ORDER BY id desc";
      $this->db->prepare($query);
      if($res = mysqli_query($this->db, $query)){
        while($row = mysqli_fetch_row($res)){
          echo '
          <div class="mix ' . $row['6']. ' popup-youtube" href="' . $row['5']. '" title="' . $row['1']. '">
          <div class="portfolio-wrapper">
            <img src="' . $row['4']. '" alt="">
            <div class="caption">
              <div class="caption-text">
                <a class="text-title">' . $row['1']. '</a>
                <span class="text-category">' . $row['2']. '</span>
                <div class="text-category">' . $row['3']. '</div>
              </div>
                <div class="caption-bg"></div>
              </div>
            </div>
          </div>';
        }
      }
    }
  }

?>

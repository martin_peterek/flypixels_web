<!DOCTYPE html>
<html lang>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
  <meta name="description" content="Zabýváme se natáčením videí různých žánrů, vytváření profesionálních leteckých záběrů a kódování webů a eshopů.">
  <meta name="keywords" content="natáčení, video, dron, letecké, letecké záběry, webdesign, web, weby">
  <meta name="author" content="Flypixels">

  <title>Flypixels - Admin</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">

  <!-- Custom fonts for this theme -->
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic'
    rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,600,500,700,800,900' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/styles.css" rel="stylesheet" type="text/css">
  <?php
    include ('scripts.php');
  ?>

</head>

<body id="page-top" class="l-page">
  <div class="l-header">
    <div class="header_title">Admin sekce Flypixels</div>
  </div>
  <div class="l-section">
    <div class="l-navbar">
      <a class="navbar_item" href="admin.php">Domů</a>
      <a class="navbar_item" href="admin.php?category">Přidat kategorii</a>
    </div>
    <div class="section">
      <?php 
        if(isset($_GET["category"])){
          include ("admin/newCategory.php");
        } else{
          include ("admin/newItem.php");
        }
      ?>
    </div>
  </div>
</body>

</html>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/tether/tether.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
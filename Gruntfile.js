module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            'css/*.css',
            './index.php',
            './admin.html'
          ]
        },
        options: {
          watchTask: true,
          server: './'
        }
      }
    },
    sass: {
      options: {
        sourceMap: false
      },
      dist: {
        files: {
          'css/styles.css': 'css/styles.scss'
        }
      }
    },
    postcss: {
      options: {
        map: false,
        processors: [
          require('autoprefixer'),
          // require('cssnano')
        ]
      },
      dist: {
        src: 'css/styles.css'
      }
    },
    watch: {
      scripts: {
        files: ['sass/**/*.scss', 'css/*.scss', 'css/styles.scss'],
        tasks: ['sass:dist', 'postcss:dist'],
        options: {
          // interrupt: true,
          reload: true,
          livereaload: true,
          spawn: false
        },
      }
    },
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-browser-sync');

  // Default task(s).
  grunt.registerTask('default', ['browserSync', 'watch']);
};

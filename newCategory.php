<div class="l-newCategory">
  <div class="newCategory_inputs">
    <form method="post">
      <div class="l-input">
        <label class="label" for="klic">Klíč</label>
        <input class="input" name="klic" id="klic" type="text">
      </div>
      <div class="l-input">
        <label class="label" for="nazev">Název</label>
        <input class="input" name="nazev" id="nazev" type="text">
      </div>
      <button class="btn_save" name="catSave" type="submit">Uložit</button>
    </form>
  </div>
  <div class="l-categoryList">
    <div class="categoryList_table">
      <div class="categoryList_header">
        <div class="categoryList_cell-header categoryList_cell-key">Klíč</div>
        <div class="categoryList_cell-header categoryList_cell-name">Název</div>
        <div class="categoryList_cell-header categoryList_cell-delete"></div>
      </div>
      <?php categoryList('table');?>
    </div>
    
  </div>
</div>